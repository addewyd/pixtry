﻿using System;
using System.IO;

namespace LoggerS
{
    public class Logger
    {
        string file;
        public Logger() { }
        public Logger(string f)
        {
            file = f;
        }
        public int Info(string ofile, string msg)
        {
            using (StreamWriter sw = File.AppendText(ofile))
            {
                sw.WriteLine("INFO " + msg);
            }
            return 0;
        }
        public int Info(string msg)
        {
            using (StreamWriter sw = File.AppendText(file))
            {
                sw.WriteLine("INFO " + msg);
            }
            return 0;
        }

    }
}
